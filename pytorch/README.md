# Getting Started

- Install dependencies

```
pip install -r requirements.txt
```

- Run the script

```
python main.py --save-model model --epochs 4 --only-conj --features 16
```