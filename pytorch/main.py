#https://github.com/pytorch/examples/blob/master/mnist/main.py
from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR
from torchsummary import summary
import pickle
import random


class Net(nn.Module):
    def __init__(self, nodes, features):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 5, 1)
        self.conv2 = nn.Conv2d(32, 64, 5, 1)
        self.conv3 = nn.Conv2d(64, 32, 5, 1)
        self.conv4 = nn.Conv2d(32, features, 3, 1)
        self.fc2 = nn.Linear(features, nodes)
        self.fc3 = nn.Linear(nodes, 10)

    def conv(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = F.max_pool2d(x, 2)
        x = self.conv3(x)
        x = F.relu(x)
        x = self.conv4(x)
        x = F.max_pool2d(x, 4)
        x = torch.sigmoid(x)
        x = torch.flatten(x, 1)
        return x

    def forward(self, x):
        x = self.conv(x)
        x = self.fc2(x)
        x = torch.sigmoid(x)
        x = self.fc3(x)
        output = torch.sigmoid(x)
        return output

#def weight_formula(weight, bias):
#    formula = ""
#    formula += str(weight[0].item()) + 'x0'
#    for i in range(1, len(weight)):
#        if weight[i] >= 0:
#            formula += ' + ' + str(weight[i].item()) + 'x' + str(i)
#        else:
#            formula += ' - ' + str(abs(weight[i].item())) + 'x' + str(i)
#    if bias >= 0:
#        formula += ' + ' + str(bias.item())
#    else:
#        formula += ' - ' + str(abs(bias.item()))
#
#    return formula


class Predicate():
    def __init__(self, clause, clause2):
        self.clause = clause
        self.clause2 = clause2

    def __call__(self, inputs):
        (i, bi, j, bj) = self.clause
        bool1 = (inputs[i] > 0.5).item() ^ (not bi)
        if j is None:
            return bool1
        else:
            bool2 = (inputs[j] > 0.5).item() ^ (not bj)
            if self.clause2 is None:
                return bool1 and bool2
            else:
                (i2, bi2, j2, bj2) = self.clause2
                bool3 = (inputs[i2] > 0.5).item() ^ (not bi2)
                bool4 = (inputs[j2] > 0.5).item() ^ (not bj2)
                return (bool1 and bool2) or (bool3 and bool4)


    def __str__(self):
        (i, bi, j, bj) = self.clause
        p1 = ('' if bi else '~') + 'x' + str(i)
        if j is None:
            return p1
        p2 = ('' if bj else '~') + 'x' + str(j)
        if self.clause2 is None:
            return p1 + " /\ " + p2
        else:
            (i2, bi2, j2, bj2) = self.clause2
            p3 = ('' if bi2 else '~') + 'x' + str(i2)
            p4 = ('' if bj2 else '~') + 'x' + str(j2)

            return f"({p1} /\ {p2}) \/ ({p3} /\ {p4})"

def log_loss(output, target, reduction='sum'):
    target_onehot = F.one_hot(target, num_classes=10)
    diff = torch.abs(output - target_onehot)
    result = - torch.log(1 - diff)
    if reduction=='sum':
        return torch.sum(result)
    return torch.mean(result)



def train(args, model, device, train_loader, optimizer, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = log_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
            if args.dry_run:
                break


def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += log_loss(output, target, reduction='sum').item()  # sum up batch loss
            pred = torch.argmax(output, dim=1)
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))


def main():
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 14)')
    parser.add_argument('--lr', type=float, default=1.0, metavar='LR',
                        help='learning rate (default: 1.0)')
    parser.add_argument('--gamma', type=float, default=0.7, metavar='M',
                        help='Learning rate step gamma (default: 0.7)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--dry-run', action='store_true', default=False,
                        help='quickly check a single pass')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--save-model', type=str, default=None,
                        help='For Saving the current Model')
    parser.add_argument('--load-model', type=str, default=None,
                        help='For loading the pre-saved Model')
    parser.add_argument('--only-conj', action='store_true', default=False)
    parser.add_argument('--features', type=int, default=16)

    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(args.seed)

    device = torch.device("cuda" if use_cuda else "cpu")

    train_kwargs = {'batch_size': args.batch_size}
    test_kwargs = {'batch_size': args.test_batch_size}
    if use_cuda:
        cuda_kwargs = {'num_workers': 1,
                       'pin_memory': True,
                       'shuffle': True}
        train_kwargs.update(cuda_kwargs)
        test_kwargs.update(cuda_kwargs)

    transform=transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
        ])
    dataset1 = datasets.MNIST('../data', train=True, download=True,
                       transform=transform)

    dataset2 = datasets.MNIST('../data', train=False,
                       transform=transform)
    train_loader = torch.utils.data.DataLoader(dataset1,**train_kwargs)
    test_loader = torch.utils.data.DataLoader(dataset2, **test_kwargs)

    num_nodes = 32
    num_features = args.features
    model = Net(num_nodes, num_features).to(device)
    if args.load_model:
        model.load_state_dict(torch.load(args.load_model))
    else:
        optimizer = optim.Adadelta(model.parameters(), lr=args.lr)

        scheduler = StepLR(optimizer, step_size=1, gamma=args.gamma)

        for epoch in range(1, args.epochs + 1):
            train(args, model, device, train_loader, optimizer, epoch)
            test(model, device, test_loader)
            scheduler.step()

        if args.save_model:
            torch.save(model.state_dict(), args.save_model)

    bool_table = []
    labels = []

    #Construct a t-f table
    with torch.no_grad():
        for data, label in dataset1:
            data = data.unsqueeze(0)
            features = torch.flatten(model.conv(data))

            bool_table.append((features > 0.5).tolist())
            labels.append(label)


    predicates = []
    # Synthesize a function to predict i
    for target in range(10):

        #Synthesize a boolean function
        #(x1, not?, not?)
        clause = (None, False, None, False)
        clause2 = None
        correct = 0

        target_indices = [i for i in range(len(labels)) if labels[i] == target]
        non_target_indices = random.sample([i for i in range(len(labels)) if labels[i] != target], len(target_indices))
        indices = target_indices + non_target_indices

        for f in range(num_features):
            for b in (True, False):
                correct_now = sum([(bool_table[n][f] ^ (not b)) == (labels[n] == target) for n in indices])
                if correct_now > correct:
                    correct = correct_now
                    clause = (f, b, None, False)

        for f1 in range(num_features):
            for b1 in (True, False):
                for f2 in range(f1 + 1, num_features):
                    for b2 in (True, False):
                        correct_now = sum([((bool_table[n][f1] ^ (not b1)) and (bool_table[n][f2] ^ (not b2))) == (labels[n] == target) for n in indices])
                        if correct_now > correct:
                            correct = correct_now
                            clause = (f1, b1, f2, b2)
        if not args.only_conj:
            for f1 in range(num_features):
                for b1 in (True, False):
                    for f2 in range(f1 + 1, num_features):
                        for b2 in (True, False):
                            for f3 in range(f1 + 1, num_features):
                                for b3 in (True, False):
                                    for f4 in range(f3 + 1, num_features):
                                        for b4 in (True, False):
                                            correct_now = sum([
                                                (((bool_table[n][f1] ^ (not b1)) and (bool_table[n][f2] ^ (not b2))) or
                                                ((bool_table[n][f3] ^ (not b3)) and (bool_table[n][f4] ^ (not b4))))
                                                == (labels[n] == target) for n in indices
                                            ])
                                            if correct_now > correct:
                                                correct = correct_now
                                                clause = (f1, b1, f2, b2)
                                                clause2 = (f3, b3, f4, b4)
        pred = Predicate(clause, clause2)
        print(f"Predicate {target} found: {pred}")
        predicates.append(pred)

    #Construct a t-f table
    correct = 0
    with torch.no_grad():
        for data, label in dataset2:
            data = data.unsqueeze(0)
            features = torch.flatten(model.conv(data))
            prediction = -1
            for i, pred in enumerate(predicates):
                if pred(features):
                    prediction = i
                    break
            correct += (prediction == label)

    print(f"Accuracy: {correct} / {len(dataset2)} ({int(correct / len(dataset2) * 100)}%)")

    with open('predicates.pkl', 'wb') as f:
        pickle.dump(predicates, f)

if __name__ == '__main__':
    main()
